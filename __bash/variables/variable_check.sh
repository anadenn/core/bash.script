function CHECK_VAR() {


    local value="$1"
 
    if [ -z "$value" ]
    then
        ERROR 1 "no value for variable $@"
    else
        MESSAGE "CHECK_VAR $value"
    fi
}
function CHECK_GLOBAL() {


    eval "local value=\"\$$1\""
 
    if [ -z "$value" ]
    then
        ERROR 1 "no value for variable $@"
    else
        MESSAGE "CHECK_GLOBAL $@=$value"
    fi
}

function CHECK_FILE() {


    if [ ! -f "$1" ]
    then
        ERROR 2 "no file $@"
    else
        MESSAGE "CHECK_FILE $1"
    fi
}

function CHECK_DIR() {


    if [ ! -d "$1" ]
    then
        ERROR 3 "no dir $@"
    else
        MESSAGE "CHECK_DIR $1"
    fi
}
