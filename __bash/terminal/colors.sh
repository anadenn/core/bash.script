
text_black="30"
text_red="31"
text_green="32"
text_orange="33"
text_blue="34"
text_magenta="35"
text_cyan="36"
text_white="37"
text_reset="\033[00m"


function text() {
    local str=""

    if [ ! -z "$1" ]
    then
        str="$str$(text_color $1)"
    fi

    if [ ! -z "$2" ]
    then
        str="$str$(text_fx $2)"
    fi

    if [ ! -z "$3" ]
    then
        str="$str$(background $3)"
    fi
    echo $str
}

function text_color() {
    eval "color=\"\$text_$1\""
    echo "\033[$color""m"
}

background_black="40"
background_red="41"
background_green="42"
background_orange="43"
background_blue="44"
background_magenta="45"
background_cyan="46"
background_white="47"

function background() {
    eval "bg=\"\$background_$1\""
    echo "\033[$bg""m"
}

fx_gras="01"
fx_underline="02"
fx_blink="03"
fx_line="04"

function text_fx() {
    echo XXXXX$1XXXX
    eval "fx=\"\$fx_$1\""
    echo "\033[$fx""m"
}

function text_reset() {

    echo -e "$text_reset"
}
